enablePlugins(JavaAppPackaging)

name := "akka-http-microservice"
organization := "com.theiterators"
version := "1.0"
scalaVersion := "2.12.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")
javaOptions += "-javaagent:/Users/michael.englert/Downloads/AppServerAgent-4.5.6.24621/javaagent.jar"
javaOptions += "-Dappdynamics.force.default.ssl.certificate.validation=false"

libraryDependencies ++= {
  val akkaV       = "2.5.16"
  val akkaHttpV   = "10.1.4"
  val scalaTestV  = "3.0.5"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV,
    "org.scalatest"     %% "scalatest" % scalaTestV % "test"
  )
}

Revolver.settings
